import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Home from './Home';
import Friends from './Friends';
import App from './App/index';
const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Friends: Friends,
  },
  {
    initialRouteName: 'Home',
  },
);

export default createAppContainer(AppNavigator);
