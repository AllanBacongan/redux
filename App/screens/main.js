import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import {actions, States} from '../store';
import {Login} from './login';
import {Button} from '../components';
import AppNavigator from '../../AppNavigator';

import Home from '../../Home';
/**
 * Main component. Display greeting when user is logged in,
 * otherwise it will display the login screen.
 *
 * @class App
 * @extends {Component}
 */
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      possibleFriends: ['Allie', 'Gator', 'Lizzie'],
      currentFriends: [],
    };
  }

  addFriend = index => {
    const {currentFriends, possibleFriends} = this.state;

    const addedFriend = possibleFriends.splice(index, 1);

    currentFriends.push(addedFriend);

    this.setState({
      currentFriends,
      possibleFriends,
    });
  };
  render() {
    const {doLogout, loggedIn, fullName} = this.props;

    if (!loggedIn) {
      return (
        <View>
          <Text>Redux Project</Text>
          <Login />
        </View>
      );
    }

    return (
      <AppNavigator
        screenProps={{
          currentFriends: this.state.currentFriends,
          possibleFriends: this.state.possibleFriends,
          addFriend: this.addFriend,
        }}
      />
    );

    return (
      <View>
        <Text>Welcome {fullName}!</Text>

        <Button
          onPress={() => {
            doLogout();
          }}>
          Logout
        </Button>
      </View>
    );
  }
}
export const Main = connect(
  // inject states to props
  (state: States) => ({
    loggedIn: state.user.loggedIn,
    fullName: state.user.fullName,
  }),

  // inject actions to props
  dispatch => ({
    doLogout: () => dispatch(actions.user.logout()),
  }),
)(App);
