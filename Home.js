import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import {connect} from 'react-redux';
import {actions, States} from './App/store';
import {Login} from './App/screens/login';
import App from './App/screens/main';

class Home extends React.Component {
  render() {
    const {doLogout, loggedIn, fullName} = this.props;

    return (
      <View style={styles.container}>
        <Text>
          {' '}
          We have {this.props.screenProps.currentFriends.length} friends!
        </Text>

        <Button
          title="Add some friends"
          onPress={() => this.props.navigation.navigate('Friends')}
        />
        <Button
          title="Log Out"
          onPress={() => {
            doLogout();
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect(
  // inject states to props
  (state: States) => ({
    loggedIn: state.user.loggedIn,
    fullName: state.user.fullName,
  }),

  // inject actions to props
  dispatch => ({
    doLogout: () => dispatch(actions.user.logout()),
  }),
)(Home);
